from setuptools import setup

setup(name='grade2 lookup scripts',
      version='1.0.2',
      description='For grade2 --lookup, download molecule details from PubChem, ChEBI',
      url='https://gitlab.com/gphl/grade2_lookup_scripts',
      author='Global Phasing Ltd',
      author_email='buster-develop@GlobalPhasing.com',
      license='MIT',
      packages=['pubchem_g2_lookup_script'],
      zip_safe=False,
      install_requires=['pytest', 'pubchempy', 'requests'],
      scripts=['pubchem_g2_lookup_script/pubchem_g2_lookup_script.py',
               'chebi_g2_download_script/chebi_g2_download_script.pl',
               'chebi_g2_download_script/ChEBIG2Download.pm',
               'chebi_g2_download_script/test_ChEBIG2Download.pl']
      )
