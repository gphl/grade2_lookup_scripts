#!/bin/sh
# Simple shell script for grade2 --lookup option
# Download MOL file for KEGG DRUG Database entry and use as input to grade2

# MIT License
#
# Copyright (c) 2022 Global Phasing Limited
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -e
BASENAME=$(basename "$0")
error () {
    echo "$BASENAME: error:" "$@" >&2
    exit 1
}
usage () {
    echo " USAGE: $BASENAME <ID>" >&2
    echo " " >&2
    echo "   <ID>             : KEGG DRUG Database ID (like \"D00109\")" >&2
    echo " " >&2
}
[ $# -eq 0 ] && usage && error "no argument given (see above)"
[ $# -gt 1 ] && usage && error "too many arguments given (see above)"

THIS_ID=$1
MOL_URL="https://www.kegg.jp/entry/-f+m+$THIS_ID"
MOL_FILE="$THIS_ID.mol"

if ! curl --no-progress-meter "$MOL_URL" -o "$MOL_FILE";  then
    error "in curl download"
fi

if [ ! -s "$MOL_FILE" ] ; then  # KEGG supplies empty file
    rm "$MOL_FILE"
    error "cannot retrieve $THIS_ID from KEGG"
fi

echo '['
echo "   \"--in\",  \"${PWD}/$MOL_FILE\","
echo "   \"--out\",  \"$THIS_ID\","
echo "   \"--database_id\",  \"$THIS_ID\", \"KEGG DRUG Database\","
echo "       \"https://www.kegg.jp/entry/$THIS_ID\""
echo ']'
