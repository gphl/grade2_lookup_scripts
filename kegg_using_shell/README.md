# `kegg_g2_lookup_script.sh`

[[_TOC_]]

## Description

[`kegg_g2_lookup_script.sh`](kegg_g2_lookup_script.sh) is a shell script for the Grade2 option 
``--lookup ID`` option that retrieves a molecule from the
[KEGG Drug Database](https://www.kegg.jp/kegg/drug/)
supplying it to Grade2. The shell script retrieves the
molecule by downloading a MOL file using the 
[curl](https://en.wikipedia.org/wiki/CURL) tool, so this
must be installed for it to work. Once this
done a JSON-encode array of Grade2 command line arguments
is written to STDOUT using `echo` commands.

[`kegg_g2_lookup_script.sh`](kegg_g2_lookup_script.sh) is provided to show a simple basic 
example of Grade2 --lookup script to understand the essentials. 
Practical Grade2 --lookup scripts that retrieve data from a corporate 
database are likely to use either direct database access or some 
kind of web service API. JSON-encoding is probably best done
using a JSON library but can be constructed manually as shown here.

## Installing and running `kegg_g2_lookup_script.sh`

* First [download `kegg_g2_lookup_script.sh`](
  https://gitlab.com/gphl/grade2_lookup_scripts/-/raw/main/kegg_using_shell/kegg_g2_lookup_script.sh?inline=false)

* Then give the downloaded script execute permissions with the `chmod` command
  like:
  ```
  $ chmod a+x ~/Downloads/kegg_g2_lookup_script.sh
  ```

* To see how the script works run it providing a KEGG Drug Database ID
  such as `D00109` as the single argument:
  ```
  $ cd /var/tmp
  $ ~/Downloads/kegg_g2_lookup_script.sh D00109
  [
     "--in",  "/var/tmp/D00109.mol",
     "--out",  "D00109",
     "--database_id",  "D00109", "KEGG DRUG Database",
         "https://www.kegg.jp/entry/D00109"
  ]
  $ ls -l /var/tmp/D00109.mol
  -rw-r--r--  1 osmart  osmart  1249 23 Oct 15:50 /var/tmp/D00109.mol
  ```
* Notice that the script downloads a MOL file from Kegg into the
  directory in which it is run. It outputs a list of Grade2 command
  line arguments:
  * The [`--in`](https://gphl.gitlab.io/grade2_docs/usage.html#in) option
    specifies the downloaded MOL file is to be used as an input. 
    Notice that the full path to the file is used so Grade2 will be
    able to locate the file.
  * The [`--out` option](https://gphl.gitlab.io/grade2_docs/usage.html#out) 
    is used to specify the output files should name names starting
    `D00109`. This means the output CIF restraint dictionary will be called
    `D00109.restraints.cif`
  * The [`--database_id` option](https://gphl.gitlab.io/grade2_docs/usage.html#database-id)
    is used so details of the molecule's database ID are recorded in the
    output CIF-restraints file (allowing display in downstream tools). 
    In this case the molecule is from
    `KEGG DRUG Database` its database ID is `D00109` and the webpage 
    https://www.kegg.jp/entry/D00109 provides information about the molecule.
  * This script does not provide either the
    [`--name`](https://gphl.gitlab.io/grade2_docs/usage.html#name) or the
    [`--systematic`](https://gphl.gitlab.io/grade2_docs/usage.html#systematic)
    options as they are not available. 

## Using `kegg_g2_lookup_script.sh` in the `grade2 --lookup` option

To try out the `kegg_g2_lookup_script.sh` working with grade2 (release
`1.3.0` or later):

* Download the script as shown in the previous section
* Make sure that BUSTER and Grade2 are properly setup so that 
  ```
  $ grade2 -checkdeps
  ...
  SUCCESS: grade2 -checkdeps indicates that everything needed to run grade2 works fine
  ```
  indicates `SUCCESS` as shown.
* Then set the environment variable `BDG_GRADE2_LIGAND_LOOKUP` to the location
  of the full path of the downloaded script.
  If you are a bash ksh or dash shell user then use a command like:
  ```
  $ export BDG_GRADE2_LIGAND_LOOKUP=~/Downloads/kegg_g2_lookup_script.sh
  ```
  But if you are a csh or tcsh shell user:
  ```
  $ setenv BDG_GRADE2_LIGAND_LOOKUP ~/Downloads/kegg_g2_lookup_script.sh
  ```
* The script will then be invoked when the `grade2 --lookup` option is used.
  For instance:
  ```
  $ grade2 --lookup D08514 --no_charging 
  ```
  will produce the output:
  ```
   ############################################################################
   ##   [grade2] ligand restraint dictionary generation
   ############################################################################
   
        Copyright (C) 2019-2022 by Global Phasing Limited
   
                  All rights reserved.
   
                  This software is proprietary to and embodies the confidential
                  technology of Global Phasing Limited (GPhL). Possession, use,
                  duplication or dissemination of the software is authorised
                  only pursuant to a valid written licence from GPhL.
   
     Version:   1.3.0rc4 <2022-10-20>
     Authors:   Smart OS, Sharff A, Holstein J, Womack TO, Flensburg C,
                Keller P, Paciorek W, Vonrhein C and Bricogne G 
   
   -----------------------------------------------------------------------------
   
   Lookup option --lookup "D08514"
   ---- Database: "KEGG DRUG Database"
   ---- Information: https://www.kegg.jp/entry/D08514
   ---- Molecule name: "None"
   WARNING: The conformation from SDF/MOL2 file is 2D, so generate XYZ coordinates.
   CHECK: Check the molecule's InChiKey against known PDB components:
   CHECK: Exact match to PDB chemical component(s):
   CHECK:   VIA https://www.rcsb.org/ligand/VIA "5-{2-ethoxy-5-[(4-methylpiperazin-1-yl)sulfonyl]phenyl}-1-methyl-3-propyl-1H,6H,7H-pyrazolo[4,3-D]pyrimidin-7-one"
   For help on checks against known PDB components, , see:  ....
   ---- https://gphl.gitlab.io/grade2_docs/faqs.html#checkpdbmatch
   Minimization with MMFF94s reduces energy from 84.29 to 2.49 kcal/mol
   Using CCDC Mogul-like geometry analysis.
   Mogul version 2021.2.0, CSD version 542, csd-python-api 3.0.8
   Mogul Data Libraries: as542be_ASER, Feb21_ASER, May21_ASER, Sep21_ASER
   Geometry Optimize coordinates against restraints using gelly ....
   ---- gelly: Took 972 steps, reducing the rms gradient to 0.05
   ---- gelly: and the rms bond deviation to 0.004 Angstroms.
   Have written CIF-format restraint dictionary to:   D08514.restraints.cif
   Have written ideal coordinates to PDB-format file: D08514.xyz.pdb
   Have written ideal coordinates to SDF-format file: D08514.xyz.sdf
   Have written ideal coordinates in  MOL2-format to: D08514.xyz.mol2
   Have written schematic 2D diagram SVG-format file: D08514.diagram.svg
   Have written 2D diagram & atom_id labels to file:  D08514.diagram.atom_labels.svg
   Suggestion: to view/edit the restraints, use one of the commands:
       coot -p D08514.xyz.pdb --dict D08514.restraints.cif
       EditREFMAC D08514.restraints.cif D08514.xyz.pdb LIG
   Normal termination (20 secs)
  ```
  * Notice the information given in the section
    ```
    Lookup option --lookup "D08514"
    ---- Database: "KEGG DRUG Database"
    ---- Information: https://www.kegg.jp/entry/D08514
    ---- Molecule name: "None"
    ```
    including the link https://www.kegg.jp/entry/D08514 to get
    information about the molecule.
  * The `CHECK` output shows KEGG DRUG Database entry D08514 
    is a known wwPDB component `VIA` https://www.rcsb.org/ligand/VIA
    (Sildenafil)
  * The output filenames start `D08514`

 
