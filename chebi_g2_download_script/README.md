# `chebi_g2_download_script.pl`

[[_TOC_]]

## Description

`chebi_g2_download_script.pl` is a Perl script for the Grade2 option 
``--lookup ID`` option that retrieves a molecule from the
[ChEBI Database](https://www.ebi.ac.uk/chebi/)
supplying it to Grade2 to produce a restraint dictionary for it.

The Perl code for retrieving data from ChEBI is based on the 
[Perl SOAP client](https://www.ebi.ac.uk/webservices/chebi/2.0/webService.jsp#Perl)
provided in the 
[ChEBI Web Services Documentation](https://www.ebi.ac.uk/chebi/webServices.do).


## Source code

The source code for the script 
[`chebi_g2_download_script.pl`](chebi_g2_download_script.pl)
and the Perl module 
[`ChEBIG2Download.pm`](ChEBIG2Download.pm)  
are included in this repo.

## Using `chebi_g2_download_script.pl` in the `grade2 --lookup` option

A working version of the script (with all the third party libraries)
is distributed alongside Grade2 as part of the BUSTER package.
This makes it easy to try the script with `grade2 --lookup`.

* Make sure that BUSTER and Grade2 are properly setup so that 
  ```
  $ grade2 -checkdeps
  ...
  SUCCESS: grade2 -checkdeps indicates that everything needed to run grade2 works fine
  ```
  indicates `SUCCESS` as shown.

* Then set the environment variable `BDG_GRADE2_LIGAND_LOOKUP` to
  `chebi_g2_download_script.pl` (there is no need to specify the full path).

  If you are a bash ksh or dash shell user then use the command:
  ```
  $ export BDG_GRADE2_LIGAND_LOOKUP=chebi_g2_download_script.pl
  ```
  But if you are a csh or tcsh shell user, use the command:
  ```
  $ setenv BDG_GRADE2_LIGAND_LOOKUP chebi_g2_download_script.pl
  ```

* Once this is done then the `--lookup` option will use the script.
  For example, to run grade2 for [`CHEBI:133199` (carboxyibuprofen)](
  https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133199):
  ```
  $ export BDG_GRADE2_LIGAND_LOOKUP=chebi_g2_download_script.pl
  $ grade2 --lookup CHEBI:133199 --no_charging                 
   ############################################################################
   ##   [grade2] ligand restraint dictionary generation
   ############################################################################
   
        Copyright (C) 2019-2022 by Global Phasing Limited
   
                  All rights reserved.
   
                  This software is proprietary to and embodies the confidential
                  technology of Global Phasing Limited (GPhL). Possession, use,
                  duplication or dissemination of the software is authorised
                  only pursuant to a valid written licence from GPhL.
   
     Version:   1.3.0rc4 <2022-10-20>
     Authors:   Smart OS, Sharff A, Holstein J, Womack TO, Flensburg C,
                Keller P, Paciorek W, Vonrhein C and Bricogne G 
   
   -----------------------------------------------------------------------------
   
   Lookup option --lookup "CHEBI:133199"
   ---- Database: "ChEBI"
   ---- Information: https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133199
   ---- Molecule name: "carboxyibuprofen"
   Systematic name set to "3-[4-(1-carboxyethyl)phenyl]-2-methylpropanoic acid"
   RDKit generated molecule and coordinates from input SMILES: C=1C=C(C=CC1C(C(=O)O)C)CC(C(=O)O)C
   CHECK: Check the molecule's InChiKey against known PDB components:
   CHECK: The input molecule does not match any existing PDB chemical component (up to 2022-10-14).
   For help on checks against known PDB components, , see:  ....
   ---- https://gphl.gitlab.io/grade2_docs/faqs.html#checkpdbmatch
   Minimization with MMFF94s reduces energy from 47.97 to 1.44 kcal/mol
   Using CCDC Mogul-like geometry analysis.
   Mogul version 2021.2.0, CSD version 542, csd-python-api 3.0.8
   Mogul Data Libraries: as542be_ASER, Feb21_ASER, May21_ASER, Sep21_ASER
   Geometry Optimize coordinates against restraints using gelly ....
   ---- gelly: Took 430 steps, reducing the rms gradient to 0.05
   ---- gelly: and the rms bond deviation to 0.002 Angstroms.
   Have written CIF-format restraint dictionary to:   chebi_133199.restraints.cif
   Have written ideal coordinates to PDB-format file: chebi_133199.xyz.pdb
   Have written ideal coordinates to SDF-format file: chebi_133199.xyz.sdf
   Have written ideal coordinates in  MOL2-format to: chebi_133199.xyz.mol2
   Have written schematic 2D diagram SVG-format file: chebi_133199.diagram.svg
   Have written 2D diagram & atom_id labels to file:  chebi_133199.diagram.atom_labels.svg
   Suggestion: to view/edit the restraints, use one of the commands:
       coot -p chebi_133199.xyz.pdb --dict chebi_133199.restraints.cif
       EditREFMAC chebi_133199.restraints.cif chebi_133199.xyz.pdb LIG
   Normal termination (13 secs)
  ```


## Running `chebi_g2_download_script.pl` in isolation
 
A working version of the script (with all the third party libraries)
is distributed alongside Grade2 as part of the BUSTER package.
If you want to try out the script yourself:

* first install and setup BUSTER.
* Make sure that BUSTER and Grade2 are properly setup so that 
  ```
  $ grade2 -checkdeps
  ...
  SUCCESS: grade2 -checkdeps indicates that everything needed to run grade2 works fine
  ```
  indicates `SUCCESS` as shown.
* The miniconda environment in which `pubchem_g2_lookup_script.py` is installed
  can be activated with command `. $BDG_home/scripts/grade2.setup`
  ```
  $ . $BDG_home/scripts/grade2.setup
  using CSD from $CSDHOME=/home/osmart/CCDC/CSD_2022/
  ```
* The script can then be run in isolation to see how it works.

  Using 
  [`CHEBI:133199` (carboxyibuprofen)](
  https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133199) 
  as an example:
* 
  ```
  (linux64) osmart@GWSdevelop:~$ chebi_g2_download_script.pl CHEBI:133199
  [
     "C=1C=C(C=CC1C(C(=O)O)C)CC(C(=O)O)C",
     "--out",
     "chebi_133199",
     "--database_id",
     "CHEBI::133199",
     "ChEBI",
     "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133199",
     "--name",
     "carboxyibuprofen",
     "--systematic",
     "3-[4-(1-carboxyethyl)phenyl]-2-methylpropanoic acid",
     "ChEBI_lookup"
  ]
  ```
  See above for the output of Grade2 when run with this molecule.
