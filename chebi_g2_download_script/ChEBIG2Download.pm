package ChEBIG2Download;
# MIT License
#
# Copyright (c) 2022 Global Phasing Limited
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;
use SOAP::Lite;
use JSON;


use Exporter qw(import);

our @EXPORT = qw(get_id_from_args download_from_chebi grade2_args
                 json_encode_list run);

sub run {
    # input argument a reference to a list of args
    # (there should a single argument the ChEBI id)
    # it then looks up the molecule at ChEBI
    # prints a JSON encoded list of grade2 command-line arguments
    my ($ref) =   @_;
    my @args = @{ $ref };
    my $cid = get_id_from_args(\@args);
    my ($smiles, $name, $systematic) = download_from_chebi($cid);
    my @grade2_args = grade2_args($cid, $smiles, $name, $systematic);
    my $json_g2_args = json_encode_list(\@grade2_args);
    print($json_g2_args)
}

sub get_id_from_args {
    my ($ref) =   @_;
    my @args = @{ $ref };
    my ($cid) = @args;
    if (not defined $cid) {
        die "ERROR you must specified a ChEBI ID like 'CHEBI:9139' or '789'\n";
    }
    $cid =~ s/CHEBI://;
    if (not $cid =~ /^\d+$/) {
        die "ERROR ChEBI IDs are composed of digits like '15347'\n";
    }
    return $cid
}

sub download_from_chebi {
    # argument: ChEBI ID
    # returns SMILES, Name and Systematic from ChEBI
    #
    # SOAP::Lite version 0.67
    # Please note: ChEBI webservices uses document/literal binding
    my ($cid) = @_;
    # from ChEBI SOAP example:
    # Setup service
    my $WSDL = 'https://www.ebi.ac.uk/webservices/chebi/2.0/webservice?wsdl';
    my $nameSpace = 'https://www.ebi.ac.uk/webservices/chebi';
    my $soap = SOAP::Lite
       -> uri($nameSpace)
       -> proxy($WSDL);
    # Setup method and parameters
    my $method = SOAP::Data->name('getCompleteEntity')
                           ->attr({xmlns => $nameSpace});
    my @params = ( SOAP::Data->name(chebiId => $cid));
    # Call method
    my $som = $soap->call($method => @params);

    my $smiles = $som->valueof('//smiles');
    unless ( $smiles ) {
        die "ERROR cannot download a SMILES string for CID '$cid'\n";
    }
    my $name = $som->valueof('//chebiAsciiName');
    $name = "" unless defined($name);
    my $iupac_names = $som->valueof('//IupacNames');
    my $systematic = %$iupac_names{data};
    $systematic = "" unless defined($systematic);
    return ($smiles, $name, $systematic);
}

sub grade2_args {
    # returns an array of grade2 command line args
    my ($cid, $smiles, $name, $systematic) = @_;
    my @args = ($smiles, "--out",  "chebi_$cid",
                "--database_id", "CHEBI::$cid",
                "ChEBI", "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:$cid",
                );
    push @args, ("--name", $name) unless ($name eq "");
    push @args, ("--systematic", $systematic, "ChEBI_lookup")
        unless ($systematic eq "");
    return @args;
}

sub json_encode_list {
    # supplied with a reference to an array
    # returns it as a human readable JSON-encoded string
    my ($ref) =   @_;
    my @list = @{ $ref };
    my $json = JSON->new->allow_nonref;
    return $json->pretty->encode( \@list );
}

1;