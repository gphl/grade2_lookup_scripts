#!/usr/bin/env perl
# MIT License
#
# Copyright (c) 2022 Global Phasing Limited
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;
use File::Basename;
use lib dirname (__FILE__);
use ChEBIG2Download qw(get_id_from_args download_from_chebi grade2_args
                       json_encode_list run);
use Test::Simple tests => 27;
use Test::Exception;
use Test::Output;

# test download_from_chebi
my ($smiles, $name, $systematic) = download_from_chebi("17790");
ok( $smiles eq "CO", "methanol SMILES");
ok( $name eq "methanol", "methanol name");
ok( $systematic eq "methanol", "methanol systematic");
my @aspirin_download = download_from_chebi("15365");
my @aspirin_expect = ("CC(=O)Oc1ccccc1C(O)=O",
                      "acetylsalicylic acid",
                      "2-(acetyloxy)benzoic acid");
ok( @aspirin_download ~~ @aspirin_expect, "aspirin*3");
my @cid_123_download = download_from_chebi("123");
ok( defined($cid_123_download[2]), "CID 123 systematic should be defined");
ok( $cid_123_download[2] eq "", "CID 123 systematic should be blank");
throws_ok { download_from_chebi("-1") }
    qr/ERROR cannot download a SMILES string for CID '-1'/,
    'download_from_chebi("-1") dies properly';

# test grade2_args
my @acetone_g2_args = grade2_args("15347", "CC(C)=O", "acetone", "propan-2-one");
my @acetone_wanted = qw(
    CC(C)=O
    --out chebi_15347
    --database_id CHEBI::15347 ChEBI https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15347
    --name acetone
    --systematic propan-2-one ChEBI_lookup);
while (my ($index, $wanted_arg) = each @acetone_wanted) {
    ok( $wanted_arg eq $acetone_g2_args[$index], "acetone g2_arg $wanted_arg");
}
my @blank_name_args = grade2_args("15347", "CC(C)=O", "", "propan-2-one");
ok( ! grep( /^--name$/, @blank_name_args ), 'if name is blank then no --name');
my @blank_systematic_args = grade2_args("15347", "CC(C)=O", "acetone", "");
ok( ! grep( /^--systematic$/, @blank_systematic_args ),
    'if systematic is blank then no --systematic');

# test json_encode_list
my @test_list = qw(abc 123);
my $wanted = "[ \"abc\", \"123\" ] ";
my $encoded_test_list = json_encode_list(\@test_list);
$encoded_test_list =~ s/\s+/ /g;
ok( $encoded_test_list eq $wanted, 'test json_encode_list');

# test get_id_from_args - supplied with a reference to list of arguments
my @args = ();
throws_ok { get_id_from_args(\@args) }
    qr/ERROR you must specified a ChEBI ID like 'CHEBI:9139' or '789'/,
    'get_id_from_args(\("")) dies properly';
@args = ("abc");
throws_ok { get_id_from_args(\@args) }
    qr/ERROR ChEBI IDs are composed of digits like '15347'/,
    'get_id_from_args(\("abc")) dies properly';
@args = ("123");
ok (get_id_from_args(\@args) eq "123", "get_id_from_args(@args)");
@args = ("CHEBI:789");
ok (get_id_from_args(\@args) eq "789", "get_id_from_args(@args)");

# test overall script run()
@args = ("15347");
$wanted = '[
   "CC(C)=O",
   "--out",
   "chebi_15347",
   "--database_id",
   "CHEBI::15347",
   "ChEBI",
   "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15347",
   "--name",
   "acetone",
   "--systematic",
   "propan-2-one",
   "ChEBI_lookup"
]
';
stdout_is(sub { run(\@args); }, $wanted, 'Test run("15347") - acetone');