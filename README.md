# Grade2 Lookup Scripts

[[_TOC_]]


## Description

This repository provides details of how to write a custom script
that can be run by the
[Grade2 ligand restraint dictionary generation tool](https://gphl.gitlab.io/grade2_docs/)
`--lookup` option. 
The `--lookup` option facilitates the retrieval of a molecule's details 
from a corporate (or public) chemical database, given its database ID,
so that Grade2 produces a restraint dictionary for it.

In addition to the documentation in this file,
this repository provides the source code for three working example scripts, 
each of which looks up a molecule's details from a different public
chemical database:
* [pubchem_g2_lookup_script.py](pubchem_g2_lookup_script) 
  A Python script that retrieves data from the PubChem Database for Grade2.
* [chebi_g2_download_script.pl](chebi_g2_download_script) 
  A Perl script that retrieves data from the ChEBI Database for Grade2.
* [kegg_g2_lookup_script.sh](kegg_using_shell)
  A shell script that retrieves data from the KEGG Drug Database for Grade2.

## Background on Grade2

Grade2 is a ligand restraint dictionary generation tool that
is distributed as part of the BUSTER refinement package
https://www.globalphasing.com/buster/ . 
For more details on Grade2 please see 
https://gphl.gitlab.io/grade2_docs/introduction.html

The Grade Web Server http://grade.globalphasing.org/ provides a way to
use Grade2 without the need to licence and install both the
BUSTER and the CSD-core packages. 


## Using the `grade2 --lookup ID` option

The `--lookup ID` option was first introduced in Grade2 
[release 1.3.0](https://gphl.gitlab.io/grade2_docs/changelog.html)
?? October 2022. The option is not included in older releases.

The `--lookup` option provides a mechanism whereby an external script
is invoked to look up details of a ligand from a database.
To use your own script, set environment variable `BDG_GRADE2_LIGAND_LOOKUP`
to the location of the script. 

By default, if `BDG_GRADE2_LIGAND_LOOKUP` is not set,
`grade2 --lookup CID` uses the script 
[`pubchem_g2_lookup_script.py`](pubchem_g2_lookup_script) that is
distributed alongside Grade2.
This means that by default `grade2 --lookup CID`  downloads ligand 
details from the PubChem database
https://pubchem.ncbi.nlm.nih.gov/ using the PubChem compound identifier `CID`
as an argument. For example, running:

    $ grade2 --lookup 123

will download details of the drug Triforin from PubChem using its CID `123`
(see https://pubchem.ncbi.nlm.nih.gov/compound/123 for the Triforin PubChem
entry). Grade2 will produce the following terminal output:

    $ grade2 --lookup 123 --just_cif
     ############################################################################
     ##   [grade2] ligand restraint dictionary generation
     ############################################################################

          Copyright (C) 2019-2022 by Global Phasing Limited

                    All rights reserved.

                    This software is proprietary to and embodies the confidential
                    technology of Global Phasing Limited (GPhL). Possession, use,
                    duplication or dissemination of the software is authorised
                    only pursuant to a valid written licence from GPhL.

       Version:   1.3.0 <2022-10-??>
       Authors:   Smart OS, Sharff A, Holstein J, Womack TO, Flensburg C,
                  Keller P, Paciorek W, Vonrhein C and Bricogne G

     -----------------------------------------------------------------------------

     Lookup option --lookup "123"
     ---- Database: "PubChem"
     ---- Information: https://pubchem.ncbi.nlm.nih.gov/compound/123
     ---- Molecule name: "Tiformin"
     Systematic name set to "4-(diaminomethylideneamino)butanamide"
     RDKit generated molecule and coordinates from input SMILES: C(CC(=O)N)CN=C(N)N
     CHECK: Check the molecule's InChiKey against known PDB components:
     CHECK: The input molecule does not match any existing PDB chemical component (up to 2022-08-26).
     For help on checks against known PDB components, , see:  ....
     ---- https://gphl.gitlab.io/grade2_docs/faqs.html#checkpdbmatch
     Minimization with MMFF94s reduces energy from -118.68 to -162.18 kcal/mol
     Using CCDC Mogul-like geometry analysis.
     Mogul version 2021.2.0, CSD version 542, csd-python-api 3.0.8
     Mogul Data Libraries: as542be_ASER, Feb21_ASER, May21_ASER, Sep21_ASER
     Geometry Optimize coordinates against restraints using gelly ....
     ---- gelly: Took 249 steps, reducing the rms gradient to 0.05
     ---- gelly: and the rms bond deviation to 0.002 Angstroms.
     Have written CIF-format restraint dictionary to:   CID_123.restraints.cif
     Normal termination (4 secs)

You can notice that the SMILES string `C(CC(=O)N)CN=C(N)N` downloaded
from PubChem is used as a starting point for the molecule.
A CIF-format restraint dictionary is output to the file `CID_123.restraints.cif`,
and this will include information about the molecule's name, its systematic
(IUPAC) name and its PubChem information webpage.

## Setting environment variable `BDG_GRADE2_LIGAND_LOOKUP`

The script invoked by  `grade2 --lookup` can be specified by
setting the environment variable `BDG_GRADE2_LIGAND_LOOKUP`.
For example, to use the script
[`chebi_g2_download_script.pl`](chebi_g2_download_script)
then, if you are a bash ksh or dash shell user:

    $ export BDG_GRADE2_LIGAND_LOOKUP=chebi_g2_download_script.pl

But if you are a csh or tcsh shell user:

    $ setenv BDG_GRADE2_LIGAND_LOOKUP chebi_g2_download_script.pl

Once this is done `grade2 --lookup ID` will look up molecule details
at ChEBI as explained in the 
[chebi_g2_download_script.pl README](chebi_g2_download_script).

Please note, that as the script 
[`chebi_g2_download_script.pl`](chebi_g2_download_script)
is distributed alongside Grade2 in the BUSTER suite it is not necessary
to specify the full path of the script when setting `BDG_GRADE2_LIGAND_LOOKUP`,
but, normally this will be necessary for custom scripts. For bash users,
a command based on the following should be used.

    $ export BDG_GRADE2_LIGAND_LOOKUP=/path/to/the/custom_script

Please see the [kegg_g2_lookup_script.sh README](kegg_using_shell)
for an example of setting `BDG_GRADE2_LIGAND_LOOKUP` with the full path in practice.

Once you have got a custom lookup script working, BUSTER provides a convenient
configuration mechanism to set `BDG_GRADE2_LIGAND_LOOKUP` for all users
at a site.
Please see the 
[BUSTER Configure](https://www.globalphasing.com/buster/manual/installation/Ubuntu1804_64bit/index.html#configure)
section of the
[BUSTER installation documentation](https://www.globalphasing.com/buster/manual/installation/).

## How the `grade2 --lookup ID` option uses a script

When the option `--lookup ID` is specified in Grade2 then the following 
procedure is used:

* The script location is set from the environment variable
  `BDG_GRADE2_LIGAND_LOOKUP` or from the default 
  [`pubchem_g2_lookup_script.py`](pubchem_g2_lookup_script)
* A temporary directory is created 
* The script is run with the argument `ID` within the temporary 
  working directory printing output to `STDOUT`. The output
  must be a JSON encoded array of grade2 command line arguments.
* The scripts' output is decoded into a list of
  [grade2 command line arguments](https://gphl.gitlab.io/grade2_docs/usage.html#command-line-arguments-for-grade2).
* Grade2 then proceeds combining the decoded list of command line
  arguments with the original user specified arguments, 
  other than `--lookup ID`.
* When the Grade2 run completes the temporary directory and any 
  files within it are removed.

The temporary directory is used so that any temporary files (for instance SDF )
downloaded in the process are cleaned up.

To understand this process let's use the example from above
running `grade2 --lookup 123 --just_cif` with the default script
[`pubchem_g2_lookup_script.py`](pubchem_g2_lookup_script). 
Grade2 runs the script with the argument `123`. To understand
what happens it is possible to manually run the script
in isolation once the Grade2 environment is set up.

    $ . $BDG_home/scripts/grade2.setup
    $ pubchem_g2_lookup_script.py 123
    [
        "C(CC(=O)N)CN=C(N)N",
        "--out",
        "CID_123",
        "--database_id",
        "CID123",
        "PubChem",
        "https://pubchem.ncbi.nlm.nih.gov/compound/123",
        "--name",
        "Tiformin",
        "--systematic",
        "4-(diaminomethylideneamino)butanamide",
        "PubChem lookup"
    ]

* The script's output is a JSON-encoded array. If you are not familiar
  with JSON then the article 
  [What is JSON? The universal data format](https://www.infoworld.com/article/3222851/what-is-json-a-better-format-for-data-exchange.html)
  explains how it provides a great data interchange format.
* In this case the array is a list of grade2 command line arguments:
  * `C(CC(=O)N)CN=C(N)N` is SMILES string from PubChem for CID 123.
    In this case the SMILES string is 
    [directly specified as a grade2 command line argument](https://gphl.gitlab.io/grade2_docs/usage.html#smiles-input)
  * The [`--out` option](https://gphl.gitlab.io/grade2_docs/usage.html#out) 
    is used to specify the output files should name names starting
    `CID_123`.
  * The [`--database_id` option](https://gphl.gitlab.io/grade2_docs/usage.html#database-id)
    is used so details of the molecule's database ID are recorded in the
    output CIF-restraints file (allowing display in downstream tools). 
    In this case the molecule is from
    `PubChem` its database ID is `CID123` and the webpage 
    https://pubchem.ncbi.nlm.nih.gov/compound/123 provides information
    about the molecule.
  * The [`--name` option](https://gphl.gitlab.io/grade2_docs/usage.html#name)
    is used to provide a human-readable name for the compound,
    in this case `Tiformin` (the Title of the PubChem entry)
  * The [`--systematic` option](https://gphl.gitlab.io/grade2_docs/usage.html#systematic)
    is used with the systematic (IUPAC) name retrieved
    from PubChem,  in this case `4-(diaminomethylideneamino)butanamide`.
    The source of the systematic name is also recorded, in this case
    `PubChem lookup`.

To see exactly how Grade2 handles the lookup process grade2 can be run
with the [`--debug` option](https://gphl.gitlab.io/grade2_docs/usage.html#debug)
and the terminal output piped to search for `LOOKUP`:

     $ grade2 --lookup 123 --just_cif --debug | grep LOOKUP
     DEBUG: LOOKUP: --lookup option to lookup the ligand from an external database
     DEBUG: LOOKUP: script used "pubchem_g2_lookup_script.py"
     DEBUG: LOOKUP: grade2 arguments list before the lookup:
     DEBUG: LOOKUP:     --lookup "123"
     DEBUG: LOOKUP:     --just_cif
     DEBUG: LOOKUP:     --debug
     DEBUG: LOOKUP: grade2 arguments after the --lookup process:
     DEBUG: LOOKUP:     "C(CC(=O)N)CN=C(N)N"
     DEBUG: LOOKUP:     --out "CID_123"
     DEBUG: LOOKUP:     --database_id "CID123" "PubChem" "https://pubchem.ncbi.nlm.nih.gov/compound/123"
     DEBUG: LOOKUP:     --name "Tiformin"
     DEBUG: LOOKUP:     --systematic "4-(diaminomethylideneamino)butanamide" "PubChem lookup"
     DEBUG: LOOKUP:     --just_cif
     DEBUG: LOOKUP:     --debug

You can see that list of options from the script are used replacing the original
`--lookup 123`.

## Writing a custom --lookup script

* It should be relatively straight forward to write a script 
  to look up details of a ligand from a Corporate database
  if there is a programmatic way to retrieve the SMILES string
  or a coordinate file in SDF or MOL2 format. 
* The script can be written in any language. This repo provides
  examples written in [Python](pubchem_g2_lookup_script),
  [Perl](chebi_g2_download_script) and [Shell Script](kegg_using_shell).
* The script (or executable) must have executable file permissions
  so that it can be run by Grade2.
* The environment variable `BDG_GRADE2_LIGAND_LOOKUP` must be
  set to the location of the script 
  [as explained above](#setting-environment-variable-bdg_grade2_ligand_lookup).
* When executed the script should print out to STDOUT
  an JSON-encoded array of Grade2 command line
  arguments.
* JSON-encoding is straightforward either using a JSON library or
  carefully crafted print statements 
  (as done in the [shell Script](kegg_using_shell)).
* Any errors should be reported to STDERR (how you do this is language dependent).
* If the script downloads a temporary coordinate file it is neatest that 
  this is output
  to the working directory the script is run in and the full path
  of the file is provided in the grade2
  [`--in` option](https://gphl.gitlab.io/grade2_docs/usage.html#in).
  This is because grade2 creates a temporary directory before running
  the script in it. This temporary directory is cleaned at the end of the
  run.
* If you hit any difficulty in writing a custom `--lookup` script please 
  let us know. Send an E-mail to buster-develop@GlobalPhasing.com and
  we will be happy to help.

## Authors

The example scripts and Grade2 were originally developed by Oliver Smart
working with Andrew Sharff, Clemens Vonrhein, Claus Flensburg and Gerard
Bricogne.


## Contributing

We welcome any questions, contributions or suggestions. 
Please send an E-mail to buster-develop@GlobalPhasing.com 

## License

All scripts are provided under the [MIT License](LICENSE).
