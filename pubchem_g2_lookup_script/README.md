# `pubchem_g2_lookup_script.py`

[[_TOC_]]

## Description

`pubchem_g2_lookup_script.py` is the script that will be run by default 
by the Grade2 option ``--lookup ID`` option. It is written in Python.
The script looks up a molecule from the PubChem open chemistry database
https://pubchem.ncbi.nlm.nih.gov/ given the "Compound CID".
It uses the excellent [PubChemPy package](https://github.com/mcs07/PubChemPy/)
that makes retrieving data from PubChem easy.

Please see this repo's top [`README.md`](../README.md) for explanations
about how `pubchem_g2_lookup_script.py` is used in practice
by the Grade2 option ``--lookup ID`` option.

## Source code

The [source code of `pubchem_g2_lookup_script.py`](pubchem_g2_lookup_script.py)
is included in this repo.

## Running `pubchem_g2_lookup_script.py` in isolation
 
A working version of the script (with all the third party libraries)
is distributed alongside Grade2 as part of the BUSTER package.
If you want to try out the script yourself:

* first install and setup BUSTER.
* Make sure that BUSTER and Grade2 are properly setup so that 
  ```
  $ grade2 -checkdeps
  ...
  SUCCESS: grade2 -checkdeps indicates that everything needed to run grade2 works fine
  ```
  indicates `SUCCESS` as shown.
* The miniconda environment in which `pubchem_g2_lookup_script.py` is installed
  can be activated with command `. $BDG_home/scripts/grade2.setup`
  ```
  $ . $BDG_home/scripts/grade2.setup
  using CSD from $CSDHOME=/home/osmart/CCDC/CSD_2022/
  ```
* The script can then be run in isolation to see how it works.

  Using CID `2244` (Aspirin) as an example: 
  ```
  (linux64) $ pubchem_g2_lookup_script.py 2244
  [
        "CC(=O)OC1=CC=CC=C1C(=O)O",
        "--out",
        "CID_2244",
        "--database_id",
        "2244",
        "PubChem",
        "https://pubchem.ncbi.nlm.nih.gov/compound/2244",
        "--name",
        "Aspirin",
        "--systematic",
        "2-acetyloxybenzoic acid",
        "PubChem lookup"
  ]
  ```

  Another test providing an invalid CID:
  ```
  (linux64) $ pubchem_g2_lookup_script.py 999999999  
  pubchem_g2_lookup_script.py: error: downloading CID 999999999 from PubChem: 'PUGREST.NotFound: No record data for CID 999999999'
  ```


