"""
Tests pubchem_g2_lookup_script methods and overall function.

Note that these are integration rather than  unit tests as actual
downloads from PubChem are made - so network access is required.
"""
import json
import os

import pytest

from pubchem_g2_lookup_script import download_pubchem_title, \
    download_pubchem_information, grade2_command_args, PubChemInformation, \
    main, download_sdf

METHANOL_INFO = PubChemInformation(cid='887',
                                   title='Methanol',
                                   systematic='methanol',
                                   url='https://pubchem.ncbi.nlm.nih.gov/'
                                       'compound/887',
                                   smiles='CO')
METHANOL_WANTED = [METHANOL_INFO.smiles,
                   '--out', f'CID_{METHANOL_INFO.cid}',
                   '--database_id', f'CID{METHANOL_INFO.cid}',
                   'PubChem', METHANOL_INFO.url,
                   '--name', METHANOL_INFO.title,
                   '--systematic', METHANOL_INFO.systematic, 'PubChem lookup']


@pytest.fixture
def chdir_to_tmpdir(tmpdir):
    os.chdir(tmpdir.strpath)


class TestMethods:
    @staticmethod
    def test_download_pubchem_information():
        info = download_pubchem_information('2244')
        assert info.cid == '2244'
        assert info.smiles == 'CC(=O)OC1=CC=CC=C1C(=O)O'
        assert info.title == 'Aspirin'
        assert info.url == 'https://pubchem.ncbi.nlm.nih.gov/compound/2244'
        assert info.systematic == '2-acetyloxybenzoic acid'

    @staticmethod
    def test_download_pubchem_title():
        assert download_pubchem_title('2244') == 'Aspirin'
        assert download_pubchem_title('123') == 'Tiformin'
        with pytest.raises(ValueError):
            download_pubchem_title('-1')

    @staticmethod
    def test_grade2_command_args_smiles():
        assert grade2_command_args(info=METHANOL_INFO,
                                   sdf_file=None) == METHANOL_WANTED

    @staticmethod
    def test_grade2_command_args_sdf():
        downloaded_sdf = 'some_file.sdf'
        wanted = ['--in', downloaded_sdf] + METHANOL_WANTED[1:]
        assert grade2_command_args(info=METHANOL_INFO,
                                   sdf_file=downloaded_sdf) == wanted

    @staticmethod
    def test_download_sdf(chdir_to_tmpdir):
        sdf = download_sdf('2244')
        assert sdf == os.path.join(os.getcwd(), 'pubchem_2244_3d.sdf')
        assert os.path.isfile(sdf)


class TestScript:
    @staticmethod
    def test_script_lookup_887_methanol(capfd):
        main(arg_list=['887'])
        stdout, _ = capfd.readouterr()
        assert 'methanol' in stdout
        # decode the JSON list
        result = json.loads(stdout)
        assert result == METHANOL_WANTED

    @staticmethod
    def test_script_lookup_887_methanol_sdf_option(capfd, chdir_to_tmpdir):
        main(arg_list=['887', '--sdf'])
        stdout, _ = capfd.readouterr()
        assert 'methanol' in stdout
        sdf = os.path.join(os.getcwd(), 'pubchem_887_3d.sdf')
        assert os.path.isfile(sdf)
        assert json.loads(stdout) == ['--in', sdf] + METHANOL_WANTED[1:]

    @staticmethod
    @pytest.mark.parametrize('e_args, message',
                             [(["-1"], 'downloading CID -1 from PubChem'),
                              (['ABC'], 'argument ID: invalid int value'),
                              (['2244', '--silly'], 'unrecognized arguments:'
                                                    ' --silly'),
                              (["--sdf", "45677"],
                               'cannot download a 3D SDF for CID 45677'
                               ' from PubChem')
                              ])
    def test_script_errors(e_args, message, capfd):
        with pytest.raises(SystemExit):
            main(arg_list=e_args)
        _, stderr = capfd.readouterr()
        assert f'error: {message}' in stderr

    @staticmethod
    def test_script_lookup_71460273_no_systematic(capfd, chdir_to_tmpdir):
        # Maitotoxin https://pubchem.ncbi.nlm.nih.gov/compound/71460273
        # is a large ligand that lacks a systematic name.
        main(arg_list=['71460273'])
        stdout, _ = capfd.readouterr()
        result = json.loads(stdout)
        assert "--systematic" not in result
        assert None not in result


