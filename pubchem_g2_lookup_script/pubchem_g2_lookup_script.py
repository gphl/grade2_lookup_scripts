#!/usr/bin/env python
# MIT License
#
# Copyright (c) 2022-2025 Global Phasing Limited
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""
Example script for grade2 --ligand_lookup option that
retrieves a ligand from PubChem from its CID.

The output is a JSON encoded list of grade2 command line arguments.
"""
import argparse
import json
import os
import sys
from typing import Tuple, NamedTuple, Optional, List
from urllib.error import URLError

import pubchempy as pcp
import requests
from requests import RequestException

DISABLE_VERIFICATION = 'BDG_GRADE2_SSL_DISABLE_VERIFICATION'


class PubChemInformation(NamedTuple):
    cid: str
    smiles: str
    url: str
    systematic: str
    title: str


def main(arg_list: Optional[List[str]] = None):
    """ main function invoked when the script is run """
    _disable_ssl_verification_if_told()
    try:
        cid, use_sdf = _process_args(arg_list)
        info = download_pubchem_information(cid=cid)
        if use_sdf:
            sdf_file = download_sdf(cid)
        else:
            sdf_file = None
    except ValueError as e_mess:
        sys.stderr.write(f'{_script_name()}: error: {e_mess}\n')
        if ("Expecting value") in str(e_mess):
            sys.stderr.write(
                    "Probably your IP address has been blocked by NCBI.\n"
                    "see https://gphl.gitlab.io/grade2_docs/faqs.html#pubchem-block\n")
        sys.exit(-1)
    except (RequestException, URLError) as e_mess:
        if 'SSL: CERTIFICATE_VERIFY_FAILED' in str(e_mess):
            e_mess = ('SSL CERTIFICATE problem. Set environment variable'
                      f' "{DISABLE_VERIFICATION}" to "yes" to avoid this.')
        sys.stderr.write(f'{_script_name()}: connection error: {e_mess}\n')
        sys.exit(-1)
    g2_args = grade2_command_args(info=info, sdf_file=sdf_file)
    print(json.dumps(g2_args, indent=4, sort_keys=True))


def _script_name():
    return os.path.basename(__file__)


def _process_args(arg_list: Optional[List[str]]) -> Tuple[str, bool]:
    """ returns the ligand's PubChem CID
    from the command line and whether SDF download to be used
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('id', metavar='ID', type=int,
                        help="the ligand's PubChem CID")
    parser.add_argument('--sdf', action='store_true',
                        help='download a 3D SDF file from PubChem'
                             ' and use this rather than the SMILES'
                             ' string as grade2 input.')
    if arg_list:
        args = parser.parse_args(args=arg_list)
    else:
        args = parser.parse_args()
    cid = str(args.id)
    return cid, args.sdf


def download_pubchem_information(cid: str) -> PubChemInformation:
    """
    Downloads information about title, smiles etc. from PubChem
    """
    try:
        compound = pcp.Compound.from_cid(int(cid))
    except (ValueError, pcp.PubChemPyError) as exc:
        raise ValueError(f'downloading CID {cid} from PubChem: {exc}')
    smiles = compound.isomeric_smiles
    url = f'https://pubchem.ncbi.nlm.nih.gov/compound/{cid}'
    systematic = compound.iupac_name
    title = download_pubchem_title(cid)
    return PubChemInformation(cid=cid, smiles=smiles, systematic=systematic,
                              title=title, url=url)


def download_pubchem_title(ligand_cid) -> str:
    """
    Download the PubChem title

    This is not supplied by pubchempy so use requests

    Raises:
        ValueError: if an invalid CID is given or there is any other
        kind of problem.
    """
    url = (f'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/'
           f'{ligand_cid}/description/JSON')
    info = requests.get(url=url).json()
    try:
        title = info['InformationList']['Information'][0]['Title']
    except KeyError:
        raise ValueError(f'problem {info} in downloading title')
    return title


def grade2_command_args(info: PubChemInformation,
                        sdf_file: Optional[str]) -> List[str]:
    """ returns list of grade2 command line arguments """
    if sdf_file:
        arg_list = ['--in', sdf_file]
    else:
        arg_list = [info.smiles]
    arg_list.extend(['--out', f'CID_{info.cid}',
                     '--database_id', f'CID{info.cid}', 'PubChem', info.url,
                     '--name', info.title])
    if info.systematic is not None:
        arg_list.extend(['--systematic', info.systematic, 'PubChem lookup'])
    return arg_list


def download_sdf(cid) -> str:
    """
    downloads the SDF file to pubchem_CID_3d.sdf
    where CID is the PubChem CID in the current working directory.

    returns the full path of the sdf file
    """
    sdf_file_name = f'pubchem_{cid}_3d.sdf'
    try:
        pcp.download(outformat='SDF',
                     path=sdf_file_name,
                     identifier=cid,
                     record_type='3d')
    except pcp.NotFoundError:
        raise ValueError(f'cannot download a 3D SDF for CID {cid} from PubChem')
    sdf_file_name = os.path.join(os.getcwd(), sdf_file_name)
    return sdf_file_name


def _disable_ssl_verification_if_told():
    """
    on some Linux installations there is a problem
    urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed
    if the environment variable is set then disable ssl verification
    """
    if os.getenv(DISABLE_VERIFICATION):
        import ssl
        # noinspection PyProtectedMember
        ssl._create_default_https_context = ssl._create_unverified_context


if __name__ == "__main__":
    main()
